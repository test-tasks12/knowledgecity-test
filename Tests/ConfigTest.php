<?php

use PHPUnit\Framework\TestCase;
use Test\Knowledgecity\Config\Config;

class ConfigTest extends TestCase
{
    private array $configKeys = [
        'rootDir',
        'db',
        'apiUser',
    ];

    public function testConfigKeys()
    {
        $this->assertEquals(array_keys(Config::getInstance()->getConfig()), $this->configKeys);
    }
}
