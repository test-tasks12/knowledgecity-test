<?php

use Test\Knowledgecity\Controllers\Api\V1\AuthController;
use Test\Knowledgecity\Controllers\Api\V1\UserController;
use Test\Knowledgecity\Services\Auth;
use Test\Knowledgecity\Services\Request;
use Test\Knowledgecity\Services\Response;

require __DIR__ . '/../vendor/autoload.php';

$routes = [
    'GET' => [
        '/users' => [
            'auth' => true,
            'controller' => UserController::class,
            'action' => 'getUsers'
        ],
    ],

    'POST' => [
        '/auth' => [
            'auth' => false,
            'controller' => AuthController::class,
            'action' => 'logIn'
        ],
    ],

    'DELETE' => [
        '/auth' => [
            'auth' => false,
            'controller' => AuthController::class,
            'action' => 'logOut'
        ]
    ],
];

header('Content-type: application/json');

$request = new Request();
if (in_array($request->method(), array_keys($routes)) && in_array($request->pathInfo(), array_keys($routes[$request->method()]))) {
    $route = $routes[$request->method()][$request->pathInfo()];
    if ($route['auth']) {
        if (Auth::isGuest($request)) {
            echo json_encode((new Response([], 401))->toArray());
            exit();
        }
    }

    $controller = new $route['controller'];
    $action = $route['action'];
    $response = $controller->$action($request);
    if ($response instanceof Response) {
        echo json_encode($response->toArray());
    } else if (is_array($response)) {
        echo json_encode($response);
    } else {
        echo $response;
    }
} else {
    echo json_encode(['error' => 'method not allowed']);
}
