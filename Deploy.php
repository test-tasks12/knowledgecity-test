<?php

use JetBrains\PhpStorm\ArrayShape;
use Test\Knowledgecity\Config\Config;
use Test\Knowledgecity\Contracts\DBContract;
use Test\Knowledgecity\DB\DB;
use Test\Knowledgecity\Helpers\Helpers;

require __DIR__ . '/vendor/autoload.php';

class Deploy
{
    private DBContract $db;
    private Config $config;

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->config = Config::getInstance();
    }

    public function __invoke(): void
    {
        echo "start create databases \n";
        $this->createDatabases();
        echo "finish create databases \n\n";
        echo "start create api user \n";
        $this->createApiUser();
        echo "finish create api user \n\n";
        echo "start create students \n";
        $this->createStudents();
        echo "finish create students \n\n";
        echo "deploy finished";
    }

    public function createDatabases(): void
    {
        $this->db->runSql(
            "CREATE TABLE IF NOT EXISTS `api_users` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `username` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
                `password` CHAR(60) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                PRIMARY KEY (`id`) USING BTREE,
                UNIQUE INDEX `username` (`username`) USING BTREE
            )
            COLLATE='utf8mb4_general_ci'
            ENGINE=InnoDB
            ;
            "
        );
        $this->db->runSql(
            "CREATE TABLE IF NOT EXISTS `groups` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
                PRIMARY KEY (`id`) USING BTREE
            )
            COLLATE='utf8mb4_general_ci'
            ENGINE=InnoDB
            ;
            "
        );
        $this->db->runSql(
            "CREATE TABLE IF NOT EXISTS `students` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `username` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
                `first_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                `last_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                `group_id` BIGINT(20) NULL DEFAULT NULL,
                PRIMARY KEY (`id`) USING BTREE,
                INDEX `FK_users_groups` (`group_id`) USING BTREE,
                CONSTRAINT `FK_users_groups` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
            )
            COLLATE='utf8mb4_general_ci'
            ENGINE=InnoDB
            ;
            "
        );

        $this->db->runSql(
            "CREATE TABLE IF NOT EXISTS `access_tokens` (
                `api_user_id` BIGINT(20) NOT NULL,
                `token` CHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
                PRIMARY KEY (`api_user_id`) USING BTREE,
                UNIQUE INDEX `token` (`token`) USING BTREE,
                CONSTRAINT `FK_access_tokens_api_users` FOREIGN KEY (`api_user_id`) REFERENCES `knowledgecity`.`api_users` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
            )
            COLLATE='utf8mb4_general_ci'
            ENGINE=InnoDB
            ;
            "
        );
    }

    public function createApiUser(): void
    {
        $userConfig = $this->config->getApiUserConfig();
        $userName = $userConfig['name'];
        $password = password_hash($userConfig['password'], PASSWORD_BCRYPT);
        $sql = "INSERT INTO api_users (username, password) VALUE ('$userName', '$password')";
        $this->db->runSql($sql);
    }

    /**
     * @throws Exception
     */
    public function createStudents(): void
    {
        $sql = "INSERT INTO `groups` (name) VALUES ('Default group'), ('Test group'), ('Good group'), ('Bad group')";
        $this->db->runSql($sql);
        for ($i = 0; $i < 50; $i++) {
            $student = $this->randomName();
            $firstname = $student['firstname'];
            $lastname = $student['lastname'];
            $username = $student['username'];
            $groupId = random_int(1, 4);
            $this->db->runSql("INSERT INTO `students` (username, first_name, last_name, group_id) VALUE ('$username', '$firstname', '$lastname', '$groupId')");
        }
    }

    /**
     * @throws Exception
     */
    #[ArrayShape([
        'firstname' => "string",
        'lastname' => "string",
        'username' => "string",
    ])]
    public function randomName(): array
    {
        $firstnames = [
            'Johnathon',
            'Anthony',
            'Erasmo',
            'Raleigh',
            'Nancie',
            'Tama',
            'Camellia',
            'Augustine',
            'Christeen',
            'Luz',
            'Diego',
            'Lyndia',
            'Thomas',
            'Georgianna',
            'Leigha',
            'Alejandro',
            'Marquis',
            'Joan',
            'Stephania',
            'Elroy',
            'Zonia',
            'Buffy',
            'Sharie',
            'Blythe',
            'Gaylene',
            'Elida',
            'Randy',
            'Margarete',
            'Margarett',
            'Dion',
            'Tomi',
            'Arden',
            'Clora',
            'Laine',
            'Becki',
            'Margherita',
            'Bong',
            'Jeanice',
            'Qiana',
            'Lawanda',
            'Rebecka',
            'Maribel',
            'Tami',
            'Yuri',
            'Michele',
            'Rubi',
            'Larisa',
            'Lloyd',
            'Tyisha',
            'Samatha',
        ];
        $lastnames = [
            'Mischke',
            'Serna',
            'Pingree',
            'Mcnaught',
            'Pepper',
            'Schildgen',
            'Mongold',
            'Wrona',
            'Geddes',
            'Lanz',
            'Fetzer',
            'Schroeder',
            'Block',
            'Mayoral',
            'Fleishman',
            'Roberie',
            'Latson',
            'Lupo',
            'Motsinger',
            'Drews',
            'Coby',
            'Redner',
            'Culton',
            'Howe',
            'Stoval',
            'Michaud',
            'Mote',
            'Menjivar',
            'Wiers',
            'Paris',
            'Grisby',
            'Noren',
            'Damron',
            'Kazmierczak',
            'Haslett',
            'Guillemette',
            'Buresh',
            'Center',
            'Kucera',
            'Catt',
            'Badon',
            'Grumbles',
            'Antes',
            'Byron',
            'Volkman',
            'Klemp',
            'Pekar',
            'Pecora',
            'Schewe',
            'Ramage',
        ];
        $firstname = $firstnames[rand(0, count($firstnames) - 1)];
        $lastname = $lastnames[rand(0, count($lastnames) - 1)];
        return [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'username' => Helpers::getRandomString(random_int(7, 15)),
        ];
    }
}

(new Deploy())();
