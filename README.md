# KnowledgeCity Full Stack Web Developer Test Task

## Install steps
<hr>

1 step
```shell
composer install
```

2 step
```
set envirements to .env file
```

3 step
```shell
php Deploy.php
```

4 step
```shell
bash ./server.sh
```
or run
```shell
cd public && php -S 127.0.0.1:8082
```

## Pages

login  
```
http://127.0.0.1:8082/login.html
```

students
```
http://127.0.0.1:8082/students.html
```
