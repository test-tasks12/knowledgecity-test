<?php

namespace Test\Knowledgecity\Services;

use JetBrains\PhpStorm\ArrayShape;

class Response
{
    public function __construct(protected array $body, protected int $status = 200)
    {

    }

    #[ArrayShape(['status' => "int", 'body' => "array"])]
    public function toArray(): array
    {
        return [
            'status' => $this->status,
            'body' => $this->body,
        ];
    }
}
