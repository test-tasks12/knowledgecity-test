<?php

namespace Test\Knowledgecity\Services;

use Test\Knowledgecity\DB\DB;
use Test\Knowledgecity\Helpers\Helpers;
use Test\Knowledgecity\Models\ApiUser;

class Auth
{
    public static function authentication(string $username, string $password): ?ApiUser
    {
        $sql = "SELECT * FROM api_users WHERE username=:username limit 1";
        $db = DB::getInstance();
        $user = $db->runSql($sql, [':username' => $username]);
        if (!$user) {
            return null;
        }
        $user = (array)$user[0];
        if (!password_verify($password, $user['password'])) {
            return null;
        }
        $apiUser = new ApiUser();
        $apiUser->id = $user['id'];
        $apiUser->username = $user['username'];
        return $apiUser;
    }

    public static function logIn(ApiUser $user): string
    {
        $sql = "INSERT INTO access_tokens (api_user_id, token) VALUES(:api_user_id, :token) ON DUPLICATE KEY UPDATE token = :token";
        $token = Helpers::getRandomString(50);
        $db = DB::getInstance();
        $db->runSql($sql, [':token' => $token, ':api_user_id' => $user->id]);
        return $token;
    }

    public static function logOut(string $token): bool
    {
        $sql = "DELETE FROM access_tokens WHERE token=:token";
        $db = DB::getInstance();
        $db->runSql($sql, [':token' => $token]);
        return true;
    }

    public static function isGuest(Request $request): bool
    {
        $cookie = $request->cookie();
        if (empty($cookie['token'])) {
            return true;
        }
        $token = $cookie['token'];

        $sql = "SELECT EXISTS(SELECT * FROM access_tokens WHERE token=:token) as exist";
        $db = DB::getInstance();
        $result = $db->runSql($sql, [':token' => $token]);
        if ($result[0]->exist !== '1') {
            return true;
        }

        return false;
    }
}
