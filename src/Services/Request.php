<?php

namespace Test\Knowledgecity\Services;

class Request
{
    /**
     * get request method
     * @return string
     */
    public function method(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * get request path info
     * @return string
     */
    public function pathInfo(): string
    {
        return $_SERVER['PATH_INFO'];
    }

    /**
     * get body params
     * @return array|null
     */
    public function body(): ?array
    {
        return json_decode(file_get_contents('php://input'), true);
    }

    /**
     * get query params
     * @return array
     */
    public function params(): array
    {
        return $_GET;
    }

    /**
     * get request cookies
     * @return array
     */
    public function cookie(): array
    {
        return $_COOKIE;
    }
}
