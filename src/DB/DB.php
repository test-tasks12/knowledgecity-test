<?php

namespace Test\Knowledgecity\DB;

use PDO;
use Test\Knowledgecity\Config\Config;
use Test\Knowledgecity\Contracts\DBContract;

class DB implements DBContract
{
    private static $instance = null;
    protected array $config;
    private PDO $dbh;

    public function __construct()
    {
        $this->config = Config::getInstance()->getDbConfig();
        $database = $this->config['database'];
        $host = $this->config['host'];
        $dsn = "mysql:dbname=$database;host=$host";
        $username = $this->config['username'];
        $password = $this->config['password'];
        try {
            $this->dbh = new PDO($dsn, $username, $password);
            $this->dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    public function runSql(string $sql, array $params = []): array|bool|\PDOStatement
    {
        $sth = $this->dbh->prepare($sql);
        foreach ($params as $param => $value) {
            match (gettype($value)) {
                'integer' => $type = PDO::PARAM_INT,
                default => $type = PDO::PARAM_STR,
            };
            $sth->bindValue($param, $value, $type);
        }
        $sth->execute();
        return $sth->fetchAll();
    }
}
