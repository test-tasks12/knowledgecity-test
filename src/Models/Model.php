<?php

namespace Test\Knowledgecity\Models;

use Test\Knowledgecity\Contracts\DBContract;
use Test\Knowledgecity\DB\DB;

abstract class Model
{
    protected DBContract $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }
}
