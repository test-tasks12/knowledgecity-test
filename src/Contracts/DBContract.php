<?php

namespace Test\Knowledgecity\Contracts;
interface DBContract
{
    public function runSql(string $sql): array|bool|\PDOStatement;
}
