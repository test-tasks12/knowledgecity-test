<?php

namespace Test\Knowledgecity\Controllers\Api\V1;

use Test\Knowledgecity\Services\Auth;
use Test\Knowledgecity\Services\Request;
use Test\Knowledgecity\Services\Response;

class AuthController
{
    public function logIn(Request $request): Response
    {
        $body = $request->body();
        $user = Auth::authentication($body['username'], $body['password']);
        if (!$user) {
            return new Response(['message' => 'invalid username or password'], 403);
        }
        $token = Auth::logIn($user);
        return new Response(['token' => $token]);
    }

    public function logOut(Request $request): Response
    {
        $cookie = $request->cookie();
        if (isset($cookie['token'])) {
            Auth::logOut($cookie['token']);
        }
        return new Response([]);
    }
}
