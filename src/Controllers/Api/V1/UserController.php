<?php

namespace Test\Knowledgecity\Controllers\Api\V1;

use Test\Knowledgecity\DB\DB;
use Test\Knowledgecity\Services\Request;
use Test\Knowledgecity\Services\Response;

class UserController
{
    /**
     * get users list
     * @param Request $request
     * @return Response
     */
    public function getUsers(Request $request): Response
    {
        $page = !empty($request->params()['page']) && is_numeric($request->params()['page']) ? (int)$request->params()['page'] : 1;
        $perPage = 5;

        $db = DB::getInstance();

        $count = $db->runSql("SELECT COUNT(*) as count FROM students");
        $count = (int)$count[0]->count;
        $offset = ($page - 1) * $perPage;
        $sql = "SELECT 
                    students.id,
                    students.username,
                    students.first_name as firstName,
                    students.last_name as lastName,
                    students.group_id,
                    g.name as groupName
                FROM students  
                    INNER JOIN `groups` g on g.id = students.group_id 
                LIMIT :limit OFFSET :offset";

        $result = $db->runSql($sql, [
            ':limit' => $perPage,
            ':offset' => $offset
        ]);

        $data = [
            'currentPage' => $page,
            'data' => $result,
            'lastPage' => ceil($count / $perPage),
            'perPage' => $perPage,
            'total' => $count,
        ];

        return new Response($data);
    }
}
