<?php

namespace Test\Knowledgecity\Config;

use Dotenv\Dotenv;
use function is_null;

class Config
{
    private static ?Config $instance = null;

    private static array $config;

    private function __construct()
    {
        $rootDir = realpath(__DIR__ . '/../../');
        $dotenv = Dotenv::createImmutable($rootDir);
        $env = $dotenv->load();
        $dotenv->required([
            'DB_HOST',
            'DB_PORT',
            'DB_DATABASE',
            'DB_USERNAME',
            'DB_PASSWORD',
            'API_USER_USERNAME',
            'API_USER_PASSWORD',
        ]);
        self::$config = [
            'rootDir' => $rootDir,
            'db' => [
                'host' => $env['DB_HOST'],
                'port' => $env['DB_PORT'],
                'database' => $env['DB_DATABASE'],
                'username' => $env['DB_USERNAME'],
                'password' => $env['DB_PASSWORD'],
            ],
            'apiUser' => [
                'name' => $env['API_USER_USERNAME'],
                'password' => $env['API_USER_PASSWORD'],
            ],
        ];
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * get all configs
     * @return array
     */
    public static function getConfig(): array
    {
        return static::$config;
    }

    /**
     * get db config
     * @return array
     */
    public function getDbConfig(): array
    {
        return self::$config['db'];
    }

    public function getApiUserConfig(): array
    {
        return self::$config['apiUser'];
    }
}
